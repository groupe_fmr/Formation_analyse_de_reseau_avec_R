Ceci est le dépôt compilant l'ensemble de la documentation, données et scripts utilisés lors du cycle de formation en 3 séances à l’analyse de réseau avec R
organisé par le groupe fmr entre octobre et novembre 2017.
Le dépôt est divisé en trois branches, une branche par séance.  Voici la liste des séances:

   * 24  octobre 2017 : « Mettre en forme et importer des données » par Marion Maisonobe et Paul Gourdon (résumé de la séance disponible [ici](http://groupefmr.hypotheses.org/4480)) ;
   * 21 novembre 2017 : « Calculer des indicateurs et explorer un graphe » par Antoine Peris et Romain Lecomte (résumé de la séance disponible [ici](http://groupefmr.hypotheses.org/4517));
   * 19 décembre 2017 : « Partitionner un graphe et filtrer l’information » par Hadrien Commenges (résumé de la séance disponible [ici]).

Les inscriptions à la liste de diffusion du groupe pour se tenir informé des prochains évènements se font en contactant Ryma Hachi (hachiryma@gmail.com).
Vous pouvez également consulter l'espace HAL du groupe qui comprend de nombreux tutoriels très utiles.
Web: [https://halshs.archives-ouvertes.fr/FMR](https://halshs.archives-ouvertes.fr/FMR)